-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Čtv 13. led 2022, 07:18
-- Verze serveru: 10.4.22-MariaDB
-- Verze PHP: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `shop`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `chutovy_profil`
--

CREATE TABLE `chutovy_profil` (
  `id` int(11) NOT NULL,
  `nazev` varchar(100) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `kava`
--

CREATE TABLE `kava` (
  `id` int(11) NOT NULL,
  `nazev` varchar(120) COLLATE utf8_czech_ci NOT NULL,
  `cena` decimal(5,2) NOT NULL,
  `popis` text COLLATE utf8_czech_ci NOT NULL,
  `vaha` int(11) NOT NULL,
  `id_prazirna` int(11) NOT NULL,
  `id_zeme` int(11) NOT NULL,
  `id_region` int(11) NOT NULL,
  `id_odruda` int(11) NOT NULL,
  `id_pouziti` int(11) NOT NULL,
  `id_zpusob_zpracovani` int(11) NOT NULL,
  `id_stupen_prazeni` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `kava_chutovy_profil`
--

CREATE TABLE `kava_chutovy_profil` (
  `id_kava` int(11) NOT NULL,
  `id_chutovy_profil` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `odruda`
--

CREATE TABLE `odruda` (
  `id` int(11) NOT NULL,
  `nazev` varchar(50) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `pouziti`
--

CREATE TABLE `pouziti` (
  `id` int(11) NOT NULL,
  `nazev` varchar(50) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `prazirna`
--

CREATE TABLE `prazirna` (
  `id` int(11) NOT NULL,
  `nazev` varchar(50) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `region`
--

CREATE TABLE `region` (
  `id` int(11) NOT NULL,
  `nazev` varchar(50) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `stupen_prazeni`
--

CREATE TABLE `stupen_prazeni` (
  `id` int(11) NOT NULL,
  `nazev` varchar(100) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `user_name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Vypisuji data pro tabulku `users`
--

INSERT INTO `users` (`id`, `user_id`, `user_name`, `password`, `date`) VALUES
(1, 25745, 'asd', 'asd', '2022-01-06 15:08:06'),
(4, 7642, 'asd', '912ec803b2ce49e4a541068d495ab570', '2022-01-12 20:38:00'),
(5, 64785996433554, 'kj', 'b3a73fbedd32a7f69daeafaa8c3d95aa', '2022-01-12 20:38:54'),
(6, 711, 'asdg', '7e6a6a87bf3ffb29a6dd9f14afdc3b88', '2022-01-12 20:39:18');

-- --------------------------------------------------------

--
-- Struktura tabulky `zeme_puvodu`
--

CREATE TABLE `zeme_puvodu` (
  `id` int(11) NOT NULL,
  `nazev` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

-- --------------------------------------------------------

--
-- Struktura tabulky `zpusob_zpracovani`
--

CREATE TABLE `zpusob_zpracovani` (
  `id` int(11) NOT NULL,
  `nazev` varchar(50) COLLATE utf8_czech_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

--
-- Indexy pro exportované tabulky
--

--
-- Indexy pro tabulku `chutovy_profil`
--
ALTER TABLE `chutovy_profil`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pro tabulku `kava`
--
ALTER TABLE `kava`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pro tabulku `odruda`
--
ALTER TABLE `odruda`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pro tabulku `pouziti`
--
ALTER TABLE `pouziti`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pro tabulku `prazirna`
--
ALTER TABLE `prazirna`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pro tabulku `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pro tabulku `stupen_prazeni`
--
ALTER TABLE `stupen_prazeni`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pro tabulku `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `date` (`date`),
  ADD KEY `user_name` (`user_name`);

--
-- Indexy pro tabulku `zeme_puvodu`
--
ALTER TABLE `zeme_puvodu`
  ADD PRIMARY KEY (`id`);

--
-- Indexy pro tabulku `zpusob_zpracovani`
--
ALTER TABLE `zpusob_zpracovani`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `chutovy_profil`
--
ALTER TABLE `chutovy_profil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `kava`
--
ALTER TABLE `kava`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `odruda`
--
ALTER TABLE `odruda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `pouziti`
--
ALTER TABLE `pouziti`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `prazirna`
--
ALTER TABLE `prazirna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `region`
--
ALTER TABLE `region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `stupen_prazeni`
--
ALTER TABLE `stupen_prazeni`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pro tabulku `zeme_puvodu`
--
ALTER TABLE `zeme_puvodu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pro tabulku `zpusob_zpracovani`
--
ALTER TABLE `zpusob_zpracovani`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
