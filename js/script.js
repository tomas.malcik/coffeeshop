let navbar = document.querySelector('.navbar');
produkty = {};

//Get all products


async function init_website() {
    //Get all products
    await GetProdukty(); //API calls take time, we have to wait for a while
    generateProducts();
}

window.onload = init_website();

// this.loadItems();
// this.distributeItems();
// loadItems = function() {
//     this.products = $.parseJSON($.ajax({
//         url: "php/api.php",
//         dataType: "json",
//         async: false
//     }).responseText);
// };
// distributeItems = function() {
//     //priradi itemy do divu
//     const product = document.querySelectorAll(".content h3");

//         product.setAttribute("nazev", this.products.nazev);

//         //smazat
//         this.products.splice(this.products.indexOf(this.products));
//         console.log(this.products);
//     }
document.querySelector('#menu-btn').onclick = () =>{
    navbar.classList.toggle('active');
    searchForm.classList.remove('active');
    cartItem.classList.remove('active');
}

let searchForm = document.querySelector('.search-form');

document.querySelector('#search-btn').onclick = () =>{
    searchForm.classList.toggle('active');
    navbar.classList.remove('active');
    cartItem.classList.remove('active');
}
 

let cartItem = document.querySelector('.cart-items-container');

document.querySelector('#cart-btn').onclick = () =>{
    cartItem.classList.toggle('active');
    navbar.classList.remove('active');
    searchForm.classList.remove('active');
    
}

window.onscroll = () =>{
    navbar.classList.remove('active');
    searchForm.classList.remove('active');
    cartItem.classList.remove('active');
}


//Functions
function generateProducts() {
    let productsString = "";
    for(let i = 0; i < produkty.length; i++) {
        productsString += product(produkty[i].id,produkty[i].nazev, produkty[i].cena, produkty[i].cena_sleva, produkty[i].avg_rating,
                                 produkty[i].sleva, produkty[i].image);
    }
    document.querySelector("#productsBox").innerHTML = productsString;
}

//API calls
async function GetProdukty() { //Asynchronous request to api.php
    res = await fetch('php/apiHandler.php?doGetAllCoffee', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      })
    .then((res) => res.json());
    produkty = res;     
}
