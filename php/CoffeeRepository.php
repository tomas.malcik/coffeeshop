<?php

require("database.class.php");


class CoffeeRepository {
    private $db;

    public $COL_ID = 'id';
    public $COL_NAZEV = 'nazev';
    public $COL_CENA = 'cena';
    public $COL_SLEVA = 'sleva';
    public $COL_CENASLEVA = 'cena_sleva';
    public $COL_IMAGE = 'image';
    public $COL_RATING = 'avg_rating';

    public function __construct() {
        $this->db = new Database();
    }

    public function getAllCoffee() {
        $query = "SELECT ".$this->COL_ID.", ".$this->COL_NAZEV.", ".$this->COL_CENA.", ".$this->COL_SLEVA.", ".$this->COL_CENASLEVA.", 
                         ".$this->COL_IMAGE.", ".$this->COL_RATING."
                 FROM coffee_with_rating";
        
        //Execute DB query
        if($re = $this->db->execute($query)) {
            if($re->num_rows > 0) {
                return $this->turnToJson($re);
            }
        }
    }

    private function turnToJson($resource) {
        $arr = array();
        while($row = $resource->fetch_assoc()) {
            $arr[] = $row;
        }
        return json_encode($arr);
    }
}

?>